<?php


namespace App\Tests\Mocks;


use App\Api\MovieApiInterface;
use App\Entity\Movie;
use App\Entity\MovieDetails;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;

class ThemoviedbApiMock implements MovieApiInterface
{
    public static $fixtures = [];
    /** @var MovieApiInterface */
    private $decorated;

    public function __construct(MovieApiInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function setHttpClient(Client $client): array
    {
        $responses = array_merge(static::$fixtures,
            [new RequestException("Error Communicating with Server", new Request('GET', 'test'))]);

        $handler = HandlerStack::create(new MockHandler($responses));
        $mockClient = new Client(['handler' => $handler]);

        $this->decorated->setHttpClient($mockClient);
    }

    public function findTopMovies(): array
    {
        $res = [];
        foreach (range(551, 560) as $i) {
            $res[] = new Movie($i, 'nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg', 'title');
        }

        return $res;
    }

    public function getMovieDetailsById(int $id): ?MovieDetails
    {
        return new MovieDetails(
            new Movie($id, 'nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg','test movie'),
            'test overview',
            new \DateTime(),
            7.1,
            100);
    }


}
