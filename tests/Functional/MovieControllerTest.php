<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MovieControllerTest extends WebTestCase
{
    public function testMovieList()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertCount(10, $crawler->filter('img[src^="https://image.tmdb.org"]'));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testMovieDetails()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/551');
        $this->assertCount(1, $crawler->filter('img[src^="https://image.tmdb.org"]'));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('html:contains("test overview")'));
    }
}