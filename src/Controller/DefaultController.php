<?php


namespace App\Controller;

use App\Api\MovieApiInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(MovieApiInterface $movieApi)
    {
        $movies = $movieApi->findTopMovies();

        return $this->render('index.html.twig', ['movies' => $movies]);
    }

    /**
     * @Route("/{id}", name="details")
     */
    public function details(string $id, MovieApiInterface $movieApi)
    {
        $details = $movieApi->getMovieDetailsById($id);
        if (!$details) {
            throw new NotFoundHttpException();
        }

        return $this->render('details.html.twig', ['details' => $details]);
    }
}
