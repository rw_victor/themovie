<?php


namespace App\Entity;


class Movie
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $poster;
    /**
     * @var string
     */
    private $title;

    public function __construct(int $id, string $poster, string $title)
    {
        $this->id = $id;
        $this->poster = $poster;
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMediumPoster(): string
    {
        return 'https://image.tmdb.org/t/p/w342/'.$this->poster;
    }

    /**
     * @return string
     */
    public function getLargePoster(): string
    {
        return 'https://image.tmdb.org/t/p/w500/'.$this->poster;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}
