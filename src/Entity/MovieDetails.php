<?php


namespace App\Entity;


class MovieDetails
{
    private $movie;
    private $overview;
    private $releaseDate;
    private $voteAverage;
    private $duration;

    public function __construct(Movie $movie, string $overview, \DateTimeInterface $releaseDate, float $voteAverage, int $duration)
    {
        $this->movie = $movie;
        $this->overview = $overview;
        $this->releaseDate = $releaseDate;
        $this->voteAverage = $voteAverage;
        $this->duration = $duration;
    }

    /**
     * @return Movie
     */
    public function getMovie(): Movie
    {
        return $this->movie;
    }

    /**
     * @return string
     */
    public function getOverview(): string
    {
        return $this->overview;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getReleaseDate(): \DateTimeInterface
    {
        return $this->releaseDate;
    }

    /**
     * @return float
     */
    public function getVoteAverage(): float
    {
        return $this->voteAverage;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }
}
