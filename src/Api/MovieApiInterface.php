<?php


namespace App\Api;


use App\Entity\Movie;
use App\Entity\MovieDetails;
use GuzzleHttp\Client;

interface MovieApiInterface
{
    public function setHttpClient(Client $client);

    /**
     * @return Movie[]
     */
    public function findTopMovies(): array;

    /**
     * @param int $id
     * @return MovieDetails
     */
    public function getMovieDetailsById(int $id): ?MovieDetails;
}
