<?php


namespace App\Api;


use App\Entity\Movie;
use App\Entity\MovieDetails;
use GuzzleHttp\Client;

class ThemoviedbApi implements MovieApiInterface
{
    private const API_ENDPOINT = 'https://api.themoviedb.org/3';
    private $apiKey;
    /** @var Client */
    private $httpClient;

    public function __construct()
    {
        $this->apiKey = getenv('THEMOVIEDB_API_KEY');
    }

    public function setHttpClient(Client $client)
    {
        $this->httpClient = $client;
    }

    public function findTopMovies(): array
    {
        $decodedJsons = $this->request("/discover/movie?api_key={$this->apiKey}&sort_by=popularity.desc&include_adult=false&page=1&language=en-US");
        $res = [];
        foreach ($decodedJsons['results'] as $result) {
            $res[] = $this->mapMovie($result);
        }

        return $res;
    }

    public function getMovieDetailsById(int $id): ?MovieDetails
    {
        $result = $this->request("/movie/{$id}?api_key={$this->apiKey}&language=en-US");

        return $this->mapDetails($result);
    }

    private function request(string $q): array
    {
        $response = $this->httpClient->get(self::API_ENDPOINT.$q,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
            ]);

        return json_decode($response->getBody()->getContents(), true);

    }

    private function mapMovie(array $data): Movie
    {
        return new Movie($data['id'], $data['poster_path'], $data['title']);
    }

    private function mapDetails(array $data): MovieDetails
    {
        $movie = $this->mapMovie($data);

        return new MovieDetails($movie, $data['overview'], new \DateTime($data['release_date']), $data['vote_average'], $data['runtime']);
    }


}
